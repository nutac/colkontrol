int channelTop=134;
void drawMenuLabels() {
  fill(g.colores[1]);
  rect(0, 0, width, channelTop-15);

  textAlign(CENTER, CENTER);
  fill(255);
  //text("VALUES", rows.get(0).pots.get(4).x, 7+g.altoCaption+g.sep*3, g.potWidth, g.altoCaption);
  text("VALUES", rows.get(0).pots.get(4).x, 13, g.potWidth, g.altoCaption);
  //text("CHANNEL", rows.get(0).captionX, 13, g.potWidth, g.altoCaption);
  //text("CHANNEL", rows.get(0).captionX, channelTop-2, g.potWidth, g.altoCaption);
  text("CHANNEL", rows.get(0).captionX, channelTop-9, g.potWidth, g.altoCaption);

  text("PRESETS", rows.get(0).captionX, presetsTop, g.potWidth, g.potHeight*0.66);
}
/////////////////////////////////////////////////////////////////////////////
int presetsTop=50;
ArrayList<Preset> presets;
void setupPresets() {
  presets=new ArrayList<Preset>();
  int y=presetsTop;
  for (int i=0; i<16; i++) {
    int x=rows.get(0).pots.get(i%8).x;
    if (i>=8) y=presetsTop+int(g.potHeight*0.33);
    Preset p=new Preset(i, x, y, g.potWidth, int(g.potHeight*0.33)-g.sep);
    presets.add(p);
  }
  setPresetBtnSelected(0);
}
int currentPresetIndex;
void setPresetBtnSelected(int index) {
  midiSendPause(200);
  for (int i=0; i<presets.size(); i++) {
    presets.get(i).setSelected(false);
  }
  presets.get(index).setSelected(true);

  saveCurrentValuesToGlobalData(currentPresetIndex);
  // Cargar los valores del nuevo preset
  loadValuesFromGlobalData(index);
  // Actualizar el preset actual
  currentPresetIndex = index;
}
void drawPresets() {
  for (int i=0; i<presets.size(); i++) {
    presets.get(i).draw();
  }
}

class Preset {
  int x, y, w, h;
  int index;
  boolean selected;
  boolean hover;
  Preset(int _index, int _x, int _y, int _w, int _h) {
    index=_index;
    x=_x;
    y=_y;
    w=_w;
    h=_h;
    selected=false;
    hover=false;
  }
  void draw() {
    fill(getColorFromIndex(0, BACKGROUND));
    if (selected) fill(getColorFromIndex(0, FOREGROUND));
    if (hover) fill(getColorFromIndex(0, ACTIVE));
    rect(x, y, w, h, 5);

    fill(255);
    textAlign(CENTER, CENTER);
    text(str(index), x, y, w, h);
  }
  void setSelected(boolean _selected) {
    selected=_selected;
  }
  void mouseMoved() {
    hover=false;
    if (mouseX>x) {
      if (mouseX<x+w) {
        if (mouseY>y) {
          if (mouseY<y+h) {
            hover=true;
          }
        }
      }
    }
  }
  void mousePressed() {
    if (mouseButton == LEFT) {
      if (clickedOnMe(x, y, w, h)) {
        setPresetBtnSelected(index);
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////
void createMenuButtons() {
  int toggleWidth=int(g.potWidth*0.5+g.sep*0.5);

  //CHANNELS
  int top=channelTop;
  for (int i=0; i<16; i++) {
    int top2=top-5;
    int col=i;
    //if (col>=8) {
    //  col-=8;
    //  top2+=g.altoCaption*0.5+g.sep;
    //}
    int x=rows.get(0).pots.get(int(col*0.5)).x;
    boolean par=(i%2!=0);
    if (par) x+=toggleWidth;
    createChannelBtn(str(i+1), x, top2);
  }
  //setChannel(1);

  //top+=g.altoCaption+g.sep*3;
  top=15;
  createButton("restore", "restore", rows.get(0).pots.get(5).x, top);
  createButton("save", "save", rows.get(0).pots.get(6).x, top);
  createButton("send", "send", rows.get(0).pots.get(7).x, top);


  cp5.addButton("MODE")
    .setLabel("MODE")
    .setBroadcast(false)
    .setValue(0)
    .setPosition(rows.get(0).pots.get(0).x-g.potWidth-g.sep, footerY+(g.potHeight*0.5)-g.altoCaption*0.5 )
    .setSize(g.potWidth-g.sep, g.altoCaption+g.sep)
    .setBroadcast(true)
    .setColorActive(getColorFromIndex(0, ACTIVE))
    .setColorForeground(getColorFromIndex(0, ACTIVE))
    .setColorBackground(getColorFromIndex(0, FOREGROUND ));
}
//////////////////////////////////////////////////////////////////////////////////////
ArrayList<Toggle> channelButtons = new ArrayList<Toggle>();
void createChannelBtn(String text, int x, int y) {
  int toggleWidth=int((g.potWidth-g.sep)*0.5);

  Toggle toggle = cp5.addToggle(text)
    .setBroadcast(false)
    .setValue(0)
    .setPosition(x, y)
    .setSize(toggleWidth, int(g.altoCaption*0.75))
    .setBroadcast(true)
    .setColorActive(getColorFromIndex(0, FOREGROUND))
    .setColorForeground(getColorFromIndex(0, ACTIVE))
    .setColorBackground(getColorFromIndex(0, BACKGROUND ));

  toggle.getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER);

  toggle.onClick(new CallbackListener() {
    public void controlEvent(CallbackEvent theEvent) {
      setChannel(int(text));
    }
  }
  );

  channelButtons.add(toggle);
}
void setChannel(int _index) {
  println("channel pretended: ", _index);
  if (_index<1) {

    _index=1;
  } else {
  }

  Toggle btnToSelect = channelButtons.get(_index-1);
  for (Toggle btn : channelButtons) {
    if (btn != btnToSelect) {
      btn.setValue(0);
    }
  }
  btnToSelect.setValue(1);
  g.channel=_index;
  //globalData.presets.get(currentPresetIndex).channel=_index;
  //println("Button " + _index + " was pressed!");
}


void createButton(String name, String label, int x, int y) {
  cp5.addButton(name)
    .setLabel(label)
    .setBroadcast(false)
    .setValue(0)
    .setPosition(x, y)
    .setSize(g.potWidth, g.altoCaption+g.sep)
    .setBroadcast(true)
    .setColorActive(getColorFromIndex(0, ACTIVE))
    .setColorForeground(getColorFromIndex(0, FOREGROUND))
    .setColorBackground(getColorFromIndex(0, BACKGROUND ));
}

void restore(int theValue) {
  println("RESTORE!");
  //loadData();
  //println(theValue);
  loadGlobalDataFromFile();
  loadValuesFromGlobalData(currentPresetIndex);
}

void send(int theValue) {
  println("SEND!");
  rows.get(0).pots.get(7).potColour=10;
}

void save(int theValue) {
  println("SAVE!");
  //saveData();

  saveCurrentValuesToGlobalData(currentPresetIndex);
  JSONObject json = globalDataToJSON();
  saveJSONObject(json, "data/new-data.json");
}
void MODE(int theValue) {
  toggleMode();
}
