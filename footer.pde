void drawFooter() {
  for (int i=0; i<rows.get(7).pots.size(); i++) {
    //fill(255, 0, 0, 100);
        fill(g.colores[1]);

    int x=rows.get(g.totalRows-1).pots.get(i).x;
    int y=rows.get(g.totalRows-1).pots.get(i).gridy+g.sep+int(g.potHeight*1.1);

    //fill(g.colores[1]);
    rect(x, y, g.potWidth, g.potHeight, 5);
  }
}


void addSliders() {
  int x, y, w, h;
  x=rows.get(g.totalRows-1).pots.get(0).x;
  y=rows.get(g.totalRows-1).pots.get(0).gridy+g.sep+int(g.potHeight*1.1);
  footerY=y;

  w=int(g.potWidth*0.20);
  h=int(g.potHeight*0.8);
  cp5.addSlider("slider")
    .setPosition(x, y)
    .setSize(w, h)
    .setRange(0, 127)
    .setValue(64)
    ;

  //cp5.getController("slider").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  //cp5.getController("slider").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
}
