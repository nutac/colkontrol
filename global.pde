

class VariablesGlobales {
  int potWidth = 80;
  //int potHeight = 98;
  //int altoCaption=40;
  int potHeight = 85;
  int altoCaption=25;
  int knobRadius=int(60*0.45);
  int sep=2;
  //int sep2=sep*3;
  int sep2=sep*4;

  int channel=-1;

  int potsPerRow=8;
  int totalRows=8;

  //int mode=KNOBS_GRID;
  int mode=SLIDERS_GRID;

  int isSwappingIndex=-1;
  
  boolean midiPaused=false;
  int midiPauseStartTime = 0;
  int midiPauseDuration=0;


  color[] colores = { #101010, #292937 };
  //color[] colores = { #202030,#101010 };

  color[] colores2 = {
    #00188f,
    #00b294,
    #e81123,
    #68217a,
    #00bcf2,
    #009e49,
    #ec008c,
    #bad80a,
    #5742f5,
    #f542c5,
    #fff110,
    #ff8c00
  };

  color editingColor=color(colores2[10], 190);
}
///////////////////////////////////////////////////////
class Rectangle {
  float x, y, w, h;
  Rectangle(float x, float y, float w, float h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  void draw() {
    rect(x, y, w, h, 5);
  }
  boolean contains(float px, float py) {
    return px > x && px < x + w && py > y && py < y + h;
  }
}
////////////////////////////////////////////////////////////////////////////////////////
GlobalData globalData = new GlobalData();
class GlobalData {
  ArrayList<PresetData> presets;

  GlobalData() {
    presets = new ArrayList<PresetData>();
    for (int i = 0; i < 16; i++) {
      presets.add(new PresetData());
    }
  }
}

class PresetData {
  ArrayList<RowData> rows;
  int channel=-1; 

  PresetData() {
    rows = new ArrayList<RowData>();
    for (int i = 0; i < 8; i++) {
      rows.add(new RowData());
    }
  }
}

class RowData {
  String caption = "Default Caption";
  int colour = color(255, 255, 255); // Color blanco por defecto
  ArrayList<PotData> pots;

  RowData() {
    pots = new ArrayList<PotData>();
    for (int i = 0; i < 8; i++) {
      pots.add(new PotData());
    }
  }
}

class PotData {
  String potCaption = "Pot";
  String sliderCaption = "Slider";
  int potValue = 0;
  int sliderValue = 0;
  int cc_knob = 0; // Puedes ajustar estos valores según lo que necesites
  int cc_slider = 0;
  int potColour = color(255, 0, 0); // Color rojo por defecto
  int sliderColour = color(0, 255, 0); // Color verde por defecto
}


////////////////////////////////////////
